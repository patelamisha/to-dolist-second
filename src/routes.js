import Home from "./components/Home.vue";
import AddTasks from "./components/AddTasks.vue";
import Update from "./components/Update.vue";
const routes = [
  { path: "/", component: Home },
  { path: "/AddTasks", component: AddTasks },
  { path: "/Update/:index", component: Update },
];
export default routes;